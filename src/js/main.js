function updateCounter() {
    var now = new Date().getTime();

    var distance = countdownDate - now;

    var d = (Math.floor(distance / (1000 * 60 * 60 * 24))).toString().split('');
    var h = (Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).toString().split('');
    var m = (Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))).toString().split('');

    $days.children[0].innerHTML = d < 10 ? 0 : d[0];
    $days.children[1].innerHTML = d < 10 ? d[0] : d[1];
    $hours.children[0].innerHTML = h < 10 ? 0 : h[0];
    $hours.children[1].innerHTML = h < 10 ? h[0] : h[1];
    $minutes.children[0].innerHTML = m < 10 ? 0 : m[0];
    $minutes.children[1].innerHTML = m < 10 ? m[0] : m[1];

    if (distance < 0) {
        clearInterval(countdown);
        $days.children[0].innerHTML = 0;
        $days.children[1].innerHTML = 0;
        $hours.children[0].innerHTML = 0;
        $hours.children[1].innerHTML = 0;
        $minutes.children[0].innerHTML = 0;
        $minutes.children[1].innerHTML = 0;
    }
}
var big;

// Set the date we're counting down to
if (typeof countdownDateString !== 'undefined') {
    var countdownDate = new Date(countdownDateString).getTime();

    var $days = document.getElementById("days"),
        $hours = document.getElementById("hours"),
        $minutes = document.getElementById("minutes");

    updateCounter();

    var countdown = setInterval(updateCounter, 60000);

    big = window.innerWidth < 1024;
    window.onload = drawLine;

    window.onresize = redraw;

    var $opened = null;

    Array.prototype.forEach.call(document.getElementsByClassName('open-target'), function (el) {
        el.addEventListener('click', function (e) {
            if ($opened)
                $opened.className = $opened.className.replace('opened', '');
            $opened = document.getElementById(this.getAttribute('href').replace('#', ''));
            $opened.className += ' opened';
            e.preventDefault();
        });
    });
}

var t;

function redraw() {
    clearTimeout(t);
    t = setTimeout(drawLine, 200);
}

function drawLine() {

    var base = 10,
        sum = 0;

    if (window.innerWidth < 1024 && big === true) {
        big = !big;

        var ul = document.getElementsByClassName('schedule')[0],
            l = ul.getElementsByTagName('li'),
            c1 = document.getElementById("c1"),
            myPoints1 = [base, 10];

        base = 10;
        sum = 0;

        c1.setAttribute('height', ul.offsetHeight);
        c1.setAttribute('width', 50);
        var ctx1 = c1.getContext('2d');

        for (var i = 0; i < l.length; i++) {
            sum += l[i].offsetHeight;
            myPoints1.push(Math.round(Math.random() * 20 + base), sum);
        }

        drawCurve(ctx1, myPoints1);

    } else if (window.innerWidth >= 1024 && big === false) {
        big = !big;

        var c2 = document.getElementById("c2"),
            myPoints2 = [base, 10];

        base = 10;
        sum = 0;

        c2.setAttribute('height', 50);
        c2.setAttribute('width', 3000);
        var ctx2 = c2.getContext('2d');

        for (var i = 0; i < 60; i++) {
            sum += 50;
            myPoints2.push(sum, Math.round(Math.random() * 20 + base));
        }

        drawCurve(ctx2, myPoints2);
    }

    function drawCurve(ctx, ptsa, tension, isClosed, numOfSegments, showPoints) {

        ctx.beginPath();

        drawLines(ctx, getCurvePoints(ptsa, tension, isClosed, numOfSegments));

        if (showPoints) {
            ctx.beginPath();
            for (var i = 0; i < ptsa.length - 1; i += 2)
                ctx.rect(ptsa[i] - 2, ptsa[i + 1] - 2, 4, 4);
        }

        ctx.lineWidth = 5;
        ctx.strokeStyle = '#7784b2';
        ctx.lineCap = 'round';
        ctx.setLineDash([5, 10]);
        ctx.stroke();
    }

    function getCurvePoints(pts, tension, isClosed, numOfSegments) {

        // use input value if provided, or use a default value
        tension = (typeof tension !== 'undefined') ? tension : 0.5;
        isClosed = isClosed ? isClosed : false;
        numOfSegments = numOfSegments ? numOfSegments : 16;

        var _pts = [], res = [],	// clone array
            x, y,			// our x,y coords
            t1x, t2x, t1y, t2y,	// tension vectors
            c1, c2, c3, c4,		// cardinal points
            st, t, i;		// steps based on num. of segments

        // clone array so we don't change the original
        //
        _pts = pts.slice(0);

        // The algorithm require a previous and next point to the actual point array.
        // Check if we will draw closed or open curve.
        // If closed, copy end points to beginning and first points to end
        // If open, duplicate first points to befinning, end points to end
        if (isClosed) {
            _pts.unshift(pts[pts.length - 1]);
            _pts.unshift(pts[pts.length - 2]);
            _pts.unshift(pts[pts.length - 1]);
            _pts.unshift(pts[pts.length - 2]);
            _pts.push(pts[0]);
            _pts.push(pts[1]);
        }
        else {
            _pts.unshift(pts[1]);	//copy 1. point and insert at beginning
            _pts.unshift(pts[0]);
            _pts.push(pts[pts.length - 2]);	//copy last point and append
            _pts.push(pts[pts.length - 1]);
        }

        // ok, lets start..

        // 1. loop goes through point array
        // 2. loop goes through each segment between the 2 pts + 1e point before and after
        for (i = 2; i < (_pts.length - 4); i += 2) {
            for (t = 0; t <= numOfSegments; t++) {

                // calc tension vectors
                t1x = (_pts[i + 2] - _pts[i - 2]) * tension;
                t2x = (_pts[i + 4] - _pts[i]) * tension;

                t1y = (_pts[i + 3] - _pts[i - 1]) * tension;
                t2y = (_pts[i + 5] - _pts[i + 1]) * tension;

                // calc step
                st = t / numOfSegments;

                // calc cardinals
                c1 = 2 * Math.pow(st, 3) - 3 * Math.pow(st, 2) + 1;
                c2 = -(2 * Math.pow(st, 3)) + 3 * Math.pow(st, 2);
                c3 = Math.pow(st, 3) - 2 * Math.pow(st, 2) + st;
                c4 = Math.pow(st, 3) - Math.pow(st, 2);

                // calc x and y cords with common control vectors
                x = c1 * _pts[i] + c2 * _pts[i + 2] + c3 * t1x + c4 * t2x;
                y = c1 * _pts[i + 1] + c2 * _pts[i + 3] + c3 * t1y + c4 * t2y;

                //store points in array
                res.push(x);
                res.push(y);

            }
        }

        return res;
    }

    function drawLines(ctx, pts) {
        ctx.moveTo(pts[0], pts[1]);
        for (i = 2; i < pts.length - 1; i += 2) ctx.lineTo(pts[i], pts[i + 1]);
    }
}
