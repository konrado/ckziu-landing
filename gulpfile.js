const gulp = require('gulp');
const gutil = require('gulp-util');
const cond = require('gulp-cond');
const del = require('del');
const {argv} = require('yargs');
const runSequence = require('run-sequence');

const merge = require('merge-stream');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

const eslint = require('gulp-eslint');

const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const mqpacker = require('css-mqpacker');
const cssnano = require('cssnano');

const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

const watchify = require('watchify');
const browserify = require('browserify');
const hmr = require('browserify-hmr');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');


// If gulp was called in the terminal with the --prod flag, set the node environment to production
if (argv.prod) {
    process.env.NODE_ENV = 'production';
}
const PROD = process.env.NODE_ENV === 'production';

// Configuration
const src = 'src';
const config = {
    port: PROD ? 8080 : 3000,
    paths: {
        baseDir: 'public',
        js: src + '/js/**/*.js',
        css: src + '/scss/**/*.scss'
    }
};

/**
 * Gulp Tasks
 **/

// Clears the contents of the dist and build folder
gulp.task('clean', function () {
    return del(['public/css/*', 'public/js/*']);
});

// Linting
gulp.task('lint', function () {
    return gulp.src(config.paths.js)
        .pipe(eslint({
            fix: true
        }))
        .pipe(eslint.format())
});

// Sass
gulp.task('css', function () {
    var processors = [
        autoprefixer({
            browsers: ['last 2 versions', '> 5%', 'Firefox ESR', 'Safari >= 8'],
            cascade: false
        }),
        mqpacker({
            sort: true
        }),
        cssnano
    ];

    return gulp.src(config.paths.css)
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(cond(!PROD, sourcemaps.init({loadMaps: true})))
        .pipe(cond(!PROD, sourcemaps.write('./')))
        .pipe(gulp.dest(config.paths.baseDir + '/css'));

});

// Bundles our JS (see the helper function at the bottom of the file)
gulp.task('app-js', bundle_a);
//gulp.task('vendor-js', bundle_v);

gulp.task('watch', function () {
    browserSync.init({
        proxy: 'ckziu.dev'
    });
    gulp.watch(config.paths.css, ['css']);
    gulp.watch(config.paths.css).on('change', browserSync.reload);
    gulp.watch(config.paths.js, ['app-js']);
    //gulp.watch(config.paths.js, ['vendor-js']);
    gulp.watch(config.paths.js).on('change', browserSync.reload);
});

// Default task, bundles the entire app and hosts it on an Express server
gulp.task('default', function (cb) {
    runSequence('clean', 'lint', 'css', 'app-js', 'watch', cb);
});

// Bundles our JS using browserify. Sourcemaps are used in development, while minification is used in production.
function bundle_a() {
    var b = browserify({
        entries: src + '/js/main.js',
        debug: true,
        plugin: PROD ? [] : [watchify],
        cache: {},
        packageCache: {},
        bundleExternal: false
    });

    return b.bundle()
        .on('error', function (err) {
            // print the error (can replace with gulp-util)
            console.log(err.message);
            // end this stream
            this.emit('end');
        })
        .pipe(source('js/main.js'))
        .pipe(buffer())
        .pipe(cond(PROD, uglify()))
        .pipe(cond(!PROD, sourcemaps.init({loadMaps: true})))
        .pipe(cond(!PROD, sourcemaps.write('./')))
        .pipe(gulp.dest(config.paths.baseDir));
}

function bundle_v() {
    var b = browserify({
        entries: src + '/js/common/vendor.js',
        debug: true,
        plugin: PROD ? [] : [watchify],
        cache: {},
        packageCache: {},
        bundleExternal: true
    });
    b.require('video.js');
    b.require('videojs-youtube');
    b.require('waypoints/lib/jquery.waypoints');
    return b.bundle()
        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
        .pipe(source('js/common/vendor.js'))
        .pipe(buffer())
        .pipe(cond(PROD, uglify()))
        .pipe(cond(!PROD, sourcemaps.init({loadMaps: true})))
        .pipe(cond(!PROD, sourcemaps.write('./')))
        .pipe(gulp.dest(config.paths.baseDir));
}

