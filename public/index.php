<?php
$dateStr = trim(file_get_contents('dateSet/date.txt'));
$dateEnd = new DateTime($dateStr);
$dateNow = new DateTime();
$i = $dateEnd->diff($dateNow);
?>

<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>CKZiU nr 2 - rekrutacja 2017</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<script type="text/javascript">var countdownDateString = '<?php echo $dateStr; ?>'</script>

<header>
    <div class="wrapper">
        <nav id="menu">
            <ul>
                <li><a href="#kierunki">Wybierz kierunek</a></li>
                <li><a href="#pliki">Pobierz pliki</a></li>
                <li><a href="#harmonogram">Harmonogram</a></li>
                <li><a href="#o-nas">O nas</a></li>
                <li><a href="#kontakt">Kontakt</a></li>
                <li id="back"><a href="#top"></a></li>
            </ul>
        </nav>
        <div class="hamb">
            <a id="top" href="#menu">
                <div>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </a>
        </div>
        <div id="row1" class="row">
            <div class="col-l">
                <div class="logo-wrapper">
                    <a href="http://ckziu2.pl">
                        <img src="img/ckziu_logo.png" alt="logo CKZiU" title="logo CKZiU" class="logo">
                    </a>
                </div>
            </div>
            <div class="col-r">
                <h4 style="display: none">Do rozpoczęcia <span class="text-red">rekrutacji</span> zostało:</h4>
                <div class="countdown" style="display:none;">
                    <div id="days">
                        <div class="digit"><?php echo $i->days < 10 ? 0 : substr($i->days, 0, 1); ?></div>
                        <div
                            class="digit"><?php echo $i->days < 10 ? substr($i->days, 0, 1) : substr($i->days, 1, 1); ?></div>
                        <div class="label">dni</div>
                    </div>
                    <div id="hours">
                        <div class="digit"><?php echo substr($i->format('%H'), 0, 1); ?></div>
                        <div class="digit"><?php echo substr($i->format('%H'), 1, 1); ?></div>
                        <div class="label">godzin</div>
                    </div>
                    <div id="minutes">
                        <div class="digit"><?php echo substr($i->format('%I'), 0, 1); ?></div>
                        <div class="digit"><?php echo substr($i->format('%I'), 1, 1); ?></div>
                        <div class="label">minut</div>
                    </div>
                </div>
                <a class="big-button red-button" href="https://nabor-pomorze.edu.com.pl/kandydat/app/" target="_blank">Rejestracja <span>on-line</span></a>
            </div>
        </div>
        <div id="row2" class="row">
            <div class="col-l">
                <h1>Twój <em class="text-green">zawód</em> to nasza <em>specjalność!</em></h1>
                <h2><em>Centrum Kształcenia Zawodowego i&nbsp;Ustawicznego Nr&nbsp;2</em> w Gdańsku</h2>
                <h3><span class="text-blue">Wybierz wymarzony</span> <em>kierunek:</em></h3>
            </div>
            <div class="col-r">
                <img src="img/header_people.png" alt="Zawody" title="Zawody" class="heading">
            </div>
        </div>
    </div>
</header>

<main>
    <section id="kierunki" class="bg-grad-1">
        <div class="wrapper">
            <div class="faculty green">
                <a class="open-target" href="#technikum-nr-8">
                    <h3 class="title">
                        Technikum Nr&nbsp;8
                    </h3>
                </a>
                <ul id="technikum-nr-8">
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/technik-analityk" target="_blank">
                            <span>technik analityk</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/technik-technologii-zywnosci" target="_blank">
                            <span>technik technologii żywności</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/technik-technologii-chemicznej" target="_blank">
                            <span>technik technologii chemicznej</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="faculty green">
                <a class="open-target" href="#technikum-nr-14">
                    <h3 class="title">
                        Technikum Nr&nbsp;14
                    </h3>
                </a>
                <ul id="technikum-nr-14">
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/technik-inzynierii-srodowiska-i-melioracji"
                           target="_blank">
                            <span>technik inżynierii środowiska i melioracji</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/technik-ochrony-srodowiska" target="_blank">
                            <span>technik ochrony środowiska</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/technik-technologii-zywnosci2" target="_blank">
                            <span>technik technologii żywności</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/technik-weterynarii-nowy-zawod" target="_blank">
                            <span>technik weterynarii</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="faculty blue">
                <a class="open-target" href="#szkola-branzowa">
                    <h3 class="title">
                        Branżowa Szkoła I stopnia nr&nbsp;8
                    </h3>
                </a>
                <ul id="szkola-branzowa">
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/cukiernik" target="_blank">
                            <span>cukiernik</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/piekarz" target="_blank">
                            <span>piekarz</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/wedliniarz" target="_blank">
                            <span>wędliniarz</span>
                        </a>
                    </li>
                    <li><a href="http://www.ckziu2.pl/index.php/operator-maszyn-i-urzadzen-przemyslu-spozywczego"
                           target="_blank">
                            <span>operator maszyn i urządzeń przemysłu spożywczego</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="faculty yellow">
                <a class="open-target" href="#szkola-policealna">
                    <h3 class="title">
                        Policealna Szkoła dla dorosłych
                    </h3>
                </a>
                <ul id="szkola-policealna">
                    <li><a href="http://www.ckziu2.pl/index.php/opiekun-osoby-starszej" target="_blank">
                            <span>opiekun osoby starszej</span>
                        </a>
                    </li>
                    <li><a href="http://www.ckziu2.pl/index.php/asystent-osoby-niepelnosprawnej" target="_blank">
                            <span>asystent osoby niepełnosprawnej</span>
                        </a>
                    </li>
                    <li><a href="http://www.ckziu2.pl/index.php/technik-turystyki-wiejskiej" target="_blank">
                            <span>technik turystyki wiejskiej</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="faculty purple">
                <a class="open-target" href="#kursy-zawodowe">
                    <h3 class="title">
                        Kwalifikacyjne kursy Zawodowe
                    </h3>
                </a>
                <ul id="kursy-zawodowe">
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/kurs-kwalifikacyjny-w-zawodzie-rolnik" target="_blank">
                            <span>rolnik</span>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.ckziu2.pl/index.php/kurs-kwalifikacyjny-w-zawodzie-cukiernik"
                           target="_blank">
                            <span>cukiernik</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section id="pliki" class="bg-grey">
        <div class="wrapper">
            <h2 class="title text-dark-blue">Ważne dokumenty</h2>
            <h4 class="subtitle text-dark-blue">do pobrania</h4>
            <ul class="files">
                <li>
                    <a href="files/WNIOSEK_O_PRZYJĘCIE_DO_TECHNIKUM.pdf" target="_blank">
                        <div class="dok-icon">
                            <div class="n"></div>
                            <div class="h"></div>
                        </div>
                        <div class="text">
                            <h3><em class="text-green">wniosek</em> o przyjęcie do Technikum</h3>
                            <p class="file-info">PDF (1,5 MB)</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="files/WNIOSEK_O_PRZYJĘCIE_DO_BRANŻOWEJ_SZKOŁY.pdf" target="_blank">
                        <div class="dok-icon">
                            <div class="n"></div>
                            <div class="h"></div>
                        </div>
                        <div class="text">
                            <h3><em class="text-green">wniosek</em> o przyjęcie do Szkoły Branżowej</h3>
                            <p class="file-info">PDF (1,6 MB)</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="files/WNIOSEK_O_PRZYJĘCIE_DO_POLICEALNEJ_SZKOŁY_DLA_DOROSŁYCH.pdf" target="_blank">
                        <div class="dok-icon">
                            <div class="n"></div>
                            <div class="h"></div>
                        </div>
                        <div class="text">
                            <h3><em class="text-green">wniosek</em> o przyjęcie do Szkoły dla dorosłych</h3>
                            <p class="file-info">PDF (1,6 MB)</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="files/KWALIFIKACYJNY_KURS_ZAWODOWY.pdf" target="_blank">
                        <div class="dok-icon">
                            <div class="n"></div>
                            <div class="h"></div>
                        </div>
                        <div class="text">
                            <h3><em class="text-green">wniosek</em> o przyjęcie na Kurs Zawodowy</h3>
                            <p class="file-info">PDF (632 KB)</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="files/regulaminrekrutacji2017_2018.pdf" target="_blank">
                        <div class="dok-icon">
                            <div class="n"></div>
                            <div class="h"></div>
                        </div>
                        <div class="text">
                            <h3><em class="text-green">regulamin</em> rekrutacji</h3>
                            <p class="file-info">PDF (7,6 MB)</p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </section>
    <section class="bg-photo" id="harmo">
        <canvas id="c2"></canvas>
        <div id="harmonogram" class="wrapper">
            <h2 class="title text-white">Harmonogram</h2>
            <h4 class="subtitle text-white">rekrutacji</h4>
            <div class="container">
                <canvas id="c1"></canvas>
                <ul class="schedule">
                    <li <?php echo $dateNow > new DateTime('2017/06/14 15:00:00') ? 'class="finished"' : ''; ?>>
                        <h5 class="text-white">Złożenie wniosku z wymaganymi dokumentami</h5>
                        <div class="cross"></div>
                        <h6 class="text-green-2">od <em class="big">8&nbsp;maja</em> do <em
                                class="big">20&nbsp;czerwca</em> godz.&nbsp;15</h6>
                    </li>
                    <li <?php echo $dateNow > new DateTime('2017/06/27 15:00:00') ? 'class="finished"' : ''; ?>>
                        <h5 class="text-white">Uzupełnienie wniosku (świadectwo ukończenia gimnazjum + zaświadczenie o
                            wyniku egzaminu gimnazjalnego)</h5>
                        <div class="cross"></div>
                        <h6 class="text-green-2">od <em class="big">23&nbsp;czerwca</em> godz.&nbsp;11 do <em
                                class="big">27&nbsp;czerwca</em> godz.&nbsp;15</h6>
                    </li>
                    <li <?php echo $dateNow > new DateTime('2017/06/29 15:00:00') ? 'class="finished"' : ''; ?>>
                        <h5 class="text-white">Weryfikacja wniosków</h5>
                        <div class="cross"></div>
                        <h6 class="text-green-2">do <em class="big">29&nbsp;czerwca</em> godz.&nbsp;15</h6>
                    </li>
                    <li <?php echo $dateNow > new DateTime('2017/07/07 12:00:00') ? 'class="finished"' : ''; ?>>
                        <h5 class="text-white">Ogłoszenie listy zakwalifikowanych</h5>
                        <div class="cross"></div>
                        <h6 class="text-green-2"><em class="big">7&nbsp;lipca</em> do godz.&nbsp;12</h6>
                    </li>
                    <li <?php echo $dateNow > new DateTime('2017/07/11 15:00:00') ? 'class="finished"' : ''; ?>>
                        <h5 class="text-white">Wydanie skierowań na badania lekarskie</h5>
                        <div class="cross"></div>
                        <h6 class="text-green-2">do <em class="big">11&nbsp;lipca</em> godz.&nbsp;15</h6>
                    </li>
                    <li <?php echo $dateNow > new DateTime('2017/07/13 15:00:00') ? 'class="finished"' : ''; ?>>
                        <h5 class="text-white">Potwierdzenie woli przyjęcia (oryginał świadectwa ukończenia gimnazjum +
                            oryginał zaświadczenia o wynikach egzaminu gimnazjalnego + zaświadczenie lekarskie)</h5>
                        <div class="cross"></div>
                        <h6 class="text-green-2">od <em class="big">10&nbsp;lipca</em> godz.&nbsp;12 do <em class="big">13&nbsp;lipca</em>
                            godz.&nbsp;15</h6>
                    </li>
                    <li <?php echo $dateNow > new DateTime('2017/07/14 12:00:00') ? 'class="finished"' : ''; ?>>
                        <h5 class="text-white">Ogłoszenie listy przyjętych</h5>
                        <div class="cross"></div>
                        <h6 class="text-green-2"><em class="big">14&nbsp;lipca</em> do godz.&nbsp;12</h6>
                    </li>
                </ul>
            </div>
        </div>

        <div class="perc-wrapper">
            <img class="shield" src="img/shield.png" alt="Perspektywy Rangking" title="Perspektywy Rangking">
            <div class="perc right bg-blue-1">
                <p class="number bg-green-1 text-white">96%</p>
                <p class="text text-white"><em>zadowolonych</em><br>absolwentów</p>
            </div>
            <div class="perc left bg-blue-1">
                <p class="text text-white"><em>zdawalności</em><br>egzaminów zawod.</p>
                <p class="number bg-green-2 text-white">98%</p>
            </div>
        </div>

        <div id="o-nas" class="wrapper">
            <div class="boxes">
                <div class="col-66">
                    <div class="row">
                        <div class="col-50 switched">
                            <div class="bg-img photo-1"></div>
                        </div>
                        <div class="col-50 text green">
                            <div>
                                <div>
                                    <h4>Nasi partnerzy</h4>
                                    <ul>
                                        <li><span>uczelnie Trójmiasta</span></li>
                                        <li><span>grupa LOTOS S.A.</span></li>
                                        <li><span>Przedsiębiorstwo "Impuls"</span></li>
                                        <li>
                                            <span>zakłady pracy branży chemicznej, spożywczej, inżynierii i ochrony środowiska</span>
                                        </li>
                                        <li><span>Cech Piekarzy i Cukierników</span></li>
                                        <li><span>Cech Rzeźników i Wędliniarzy</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-50">
                            <div class="bg-img photo-2"></div>
                        </div>
                        <div class="col-50 text blue">
                            <div>
                                <div>
                                    <h4>Specjalistyczne wyposażenie</h4>
                                    <ul>
                                        <li><span>laboratoria</span></li>
                                        <li><span>pracownie technologiczne</span></li>
                                        <li><span>nowoczesna <em>hala sportowa</em></span></li>
                                        <li><span>boisko z certyfikatem <em>FIFA</em></span></li>
                                        <li><span><em>siłownia</em></span></li>
                                        <li><span>sala do aerobiku</span></li>
                                        <li><span><em>basen</em> (od 2018 roku)</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-33">
                    <div class="row text red switched">
                        <div>
                            <div>
                                <h4>Aktywności dodatkowe</h4>
                                <ul>
                                    <li><span><em>koła zainteresowań</em></span></li>
                                    <li><span>zajęcia pozalekcyjne</span></li>
                                    <li>
                                        <span>bezpłatne zajęcia przygotowujące do <em>egzaminu maturalnego</em>/zawodowego</span>
                                    </li>
                                    <li><span>zajęcia z doradztwa zawodowego</span></li>
                                    <li><span>zajęcia <em>sportowe</em></span></li>
                                    <li><span>wolonatriat</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="bg-img photo-3"></div>
                    </div>

                </div>
            </div>
            <div class="row abs">
                <div class="col-33 text white bg-mosaic-green equal">
                    <div>
                        <h4>Gwarantowane<br>praktyki!</h4>
                        <p>20 współpracujących placówek i zakładów!</p>
                    </div>
                </div>
                <div class="col-33 text white bg-mosaic-blue equal">
                    <div>
                        <h4>Programy i&nbsp;certyfikaty międzynarodowe</h4>
                        <p>Erasmus+, Globe oraz Europass</p>
                    </div>
                </div>

                <div class="col-33 text black bg-mosaic-yellow equal">
                    <div>
                        <h4>Internat<br>i&nbsp;stołówka</h4>
                        <p>do dyspozycji uczniów</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="kontakt" class="contact bg-icons">
        <div class="wrapper">
            <h2 class="title text-dark-blue">Masz pytania?</h2>
            <h4 class="subtitle text-dark-blue">Skontaktuj się z nami!</h4>
        </div>
        <div class="wrapper">
            <form action="formSend/mail.php" method="post">
                <div class="col-50">
                    <input type="text" name="name" id="name" placeholder="imię">
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <div class="msg-container">
                        <?php if (isset($_GET['r'])) : ?>
                            <?php echo $_GET['r'] ? '<div class="success">Dziękujemy za przesłanie wiadomości</div>' : '<div class="error">Nie udało się wysłać wiadomości</div>'; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-50">
                    <textarea name="message" id="message" cols="30" rows="10" placeholder="wpisz wiadomość"></textarea>
                </div>
                <div>
                    <input type="submit" value="wyślij wiadomość">
                </div>
            </form>
        </div>
    </section>
    <section id="transport">
        <div class="wrapper">
            <div class="col-33">
                <div>
                    <div class="icon"></div>
                    <p class="text text-white"><em>z Dworca Gdańsk Główny</em><br>151, 189, 200, 205, 207, 210, 232</p>
                </div>
            </div>
            <div class="col-33">
                <div>
                    <div class="icon"></div>
                    <p class="text text-white"><em>z Pruszcza Gdańskiego</em><br>3, 50, 200, 205, 207, 232</p>
                </div>
            </div>
            <div class="col-33">
                <div>
                    <div class="icon"></div>
                    <p class="text text-white"><em>pociągami SKM i PR</em><br>do stacji Gdańsk Orunia</p>
                </div>
            </div>
        </div>
    </section>
    <div id="map"></div>

</main>


<footer class="bg-blue-2">
    <div class="wrapper">
        <p class="text-white">
            Szczegóły na <a href="http://www.ckziu2gdansk.pl/rekrutacja" target="_blank">www.ckziu2gdansk.pl/rekrutacja</a><br>
            oraz na naszym <img src="img/fb.png">
            <a href="https://www.facebook.com/ckziu2gdansk/" target="_blank">www.facebook.com/ckziu2gdansk/</a>
        </p>
    </div>
    <div class="line"></div>
    <div class="wrapper">
        <p class="text-white">Centrum Kształcenia Zawodowego<br>i Ustawicznego Nr 2 w Gdańsku</p>
        <p class="text-white small">wszelkie prawa zastrzeżone 2017</p>
    </div>
</footer>

<script>
    mapStyle = [
        {
            "featureType": "landscape",
            "stylers": [
                {
                    "hue": "#FFA800"
                },
                {
                    "saturation": 0
                },
                {
                    "lightness": 0
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.highway",
            "stylers": [
                {
                    "hue": "#53FF00"
                },
                {
                    "saturation": -73
                },
                {
                    "lightness": 40
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "stylers": [
                {
                    "hue": "#FBFF00"
                },
                {
                    "saturation": 0
                },
                {
                    "lightness": 0
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.local",
            "stylers": [
                {
                    "hue": "#00FFFD"
                },
                {
                    "saturation": 0
                },
                {
                    "lightness": 30
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "water",
            "stylers": [
                {
                    "hue": "#00BFFF"
                },
                {
                    "saturation": 6
                },
                {
                    "lightness": 8
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                {
                    "hue": "#679714"
                },
                {
                    "saturation": 33.4
                },
                {
                    "lightness": -25.4
                },
                {
                    "gamma": 1
                }
            ]
        }
    ];

    function initMap() {
        var ckziu1 = new google.maps.LatLng(
            54.3200781,
            18.6358866
        ),
            ckziu2 = new google.maps.LatLng(
            54.3202781,
            18.6368866
        );
        var map = new google.maps.Map(document.getElementById('map'), {
            scrollwheel: false,
            minZoom: 7,
            styles: mapStyle,
            zoom: 17,
            center: ckziu1
        });

        var marker1 = new google.maps.Marker({
            position: ckziu1,
            icon: 'img/marker.png',
            map: map
        });

        var marker2 = new google.maps.Marker({
            position: ckziu2,
            icon: 'img/marker.png',
            map: map
        });
    }
</script>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCFJ_qLUIboPMeCSu49KVpn0FybUT0CtBc&callback=initMap"></script>
<script
    src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g="
    crossorigin="anonymous"></script>
<script src="js/main.js"></script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-98320314-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
